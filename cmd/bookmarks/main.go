package main

import (
	_ "github.com/joho/godotenv/autoload"

	"bookmarks/internal/app"
)

func main() {
	app.Run()
}