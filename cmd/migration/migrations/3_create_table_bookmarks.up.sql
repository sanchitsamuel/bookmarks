CREATE TABLE IF NOT EXISTS bookmarks(
    id BIGSERIAL PRIMARY KEY,
    title VARCHAR(50) NOT NULL,
    link VARCHAR(255) NOT NULL,
    user_id BIGINT NOT NULL REFERENCES users,
    created_at timestamptz  NOT NULL,
    updated_at timestamptz,
    deleted_at timestamptz,
    UNIQUE (title, user_id)
);
