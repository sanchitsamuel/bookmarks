package main

import (
	"flag"
	"os"
	"fmt"
	"io/ioutil"
	"path/filepath"
	_ "github.com/joho/godotenv/autoload"

	"bookmarks/internal/database"
)

const (
	dir = "cmd/migration/migrations"
)


var (
	up   bool
	down bool
)

func main() {
	flag.BoolVar(&up, "up", false, "involves creating new tables, columns, or other database structures")
	flag.BoolVar(&down, "down", false, "involves dropping tables, columns, or other structures")
	flag.Parse()

	action := "up"
	if down {
		action = "down"
	}

	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	files, err := filepath.Glob(filepath.Join(cwd, dir, fmt.Sprintf("*.%s.sql", action)))
	if err != nil {
		panic("error when get files name")
	}

	for _, file := range files {
		data, err := ioutil.ReadFile(file)
		if err != nil {
			panic("error when read file")
		}

		fmt.Println(string(data))

		result := database.Connection().Exec(string(data))
		if result.Error != nil {
			fmt.Println(result.Error)
			panic(fmt.Sprintf("error when exec query in file: %v", file))
		}
	}
}