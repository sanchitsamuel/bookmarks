INSERT INTO users 
  (id, name, email, password, created_at, updated_at) 
  SELECT 1, 'test_user', 'user@test.com', 'qwerty', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP WHERE NOT EXISTS 
  (SELECT id FROM users WHERE id = 1);

INSERT INTO bookmarks 
  (id, user_id, title, link, created_at, updated_at) 
  SELECT 1, 1, 'Website', 'http://www.website.com', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP WHERE NOT EXISTS 
  (SELECT id FROM bookmarks WHERE id = 1);
