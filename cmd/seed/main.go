package main

import (
	"fmt"
	"os"
	"path/filepath"
	"io/ioutil"

	_ "github.com/joho/godotenv/autoload"

	"bookmarks/internal/database"
)

const (
	seedFile = "cmd/seed/seed.sql"
)

func main() {
	cwd, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	filePath := filepath.Join(cwd, seedFile)
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic("error when read file")
	}

	fmt.Println(filePath)
	fmt.Println(string(data))

	result := database.Connection().Exec(string(data))
	if result.Error != nil {
		fmt.Println(result.Error)
		panic(fmt.Sprintf("Error when exec query in file: %v", filePath))
	}
}
