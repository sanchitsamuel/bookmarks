package database

import (
  "gorm.io/driver/postgres"
  "gorm.io/gorm"
	"fmt"

	"bookmarks/internal/config"
)

var db *gorm.DB

func connectToDatabase() *gorm.DB {
	config := config.GetDBConfig()
	fmt.Println(config)
	dsn := fmt.Sprintf("host=%v user=%v password=%v dbname=%v port=5432 sslmode=disable", config.Host, config.User, config.Password, config.Name)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("Unable to connect to db")
	}

	return db.Debug()
}

func Connection() *gorm.DB {
	if db == nil {
		db = connectToDatabase()
	}

	return db
}
