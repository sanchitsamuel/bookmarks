package http

import(
	"github.com/gin-gonic/gin"

	handler "bookmarks/internal/transport/http/handlers"
	"bookmarks/internal/transport/http/middleware"
)

func Router() *gin.Engine {
	r := gin.Default()

	r.POST("/auth/login", handler.Login)
	r.POST("/auth/signup", handler.Signup)

	bookmarkRoutes := r.Group("/bookmarks", middleware.Authenticate())
	{
		bookmarkRoutes.GET("/", handler.Bookmarks)
		bookmarkRoutes.POST("/", handler.CreateBookmarks)
		bookmarkRoutes.GET("/:id", handler.Bookmark)
		bookmarkRoutes.PUT("/:id", handler.UpdateBookmarks)
		bookmarkRoutes.DELETE("/:id", handler.DeleteBookmarks)
	}

	return r
}
