package handlers

import (
	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"

	model "bookmarks/internal/models"
	"bookmarks/internal/database"
	service "bookmarks/internal/services"
)

func Login(c *gin.Context) {
	loginRequest := model.Login{}
	bindWithObject(&loginRequest, c)
	if err := loginRequest.Validate(); len(err) > 0 {
		NewApiError("Invalid email or password").RenderAuthorized(c)
		return
	}

	user := model.User{}	
	result := database.Connection().Where("email = ?", loginRequest.Email).First(&user)

	if result.Error != nil || result.RowsAffected == 0 {
		NewApiError("Invalid email or password").RenderAuthorized(c)
		return
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(loginRequest.Password)); err != nil {
		NewApiError("Invalid email or password").RenderAuthorized(c)
		return
	}

	token, err := service.CreateToken(&user)
	if err != nil {
		NewApiError("Something went wrong").RenderInternalServer(c)
		return
	}

	c.JSON(200, gin.H{
		"token": token,
		"name": user.Name,
		"email": user.Email,
	})
}

func Signup(c *gin.Context)  {
	var signup model.Signup
	bindWithObject(&signup, c)
	if err := signup.Validate(); len(err) > 0 {
		err.RenderBadRequest(c)
		return
	}

	user := model.User{
		Name: signup.Name,
		Email: signup.Email,
		Password: signup.Password,
	}

	result := database.Connection().Create(&user)
	if result.Error != nil {
		NewApiError(result.Error.Error()).RenderBadRequest(c)
		return
	}

	c.JSON(201, gin.H{
		"data": user,
	})
}
