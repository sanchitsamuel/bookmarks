package handlers

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/joho/godotenv"

	model "bookmarks/internal/models"
	"bookmarks/internal/database"
	app "bookmarks/internal/transport/http"
	service "bookmarks/internal/services"
)

func seededUser(user *model.User) {
	result := database.Connection().Where("id = ?", "1").First(&user)
	if result.Error != nil || result.RowsAffected == 0 {
		panic("No seeded user found, run test db seeds and try again.")
		return
	}

}

func TestBookmarks(t *testing.T) {
	godotenv.Load()

	user := model.User{}	
	seededUser(&user)

	router := app.Router()

	w := httptest.NewRecorder()

	token, _ := service.CreateToken(&user)
	req, _ := http.NewRequest("GET", "/bookmarks", nil)
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	router.ServeHTTP(w, req)

	assert.Equal(t, 200, w.Code)
}
