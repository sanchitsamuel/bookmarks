package handlers

import (
	"github.com/gin-gonic/gin"
)

func bindWithObject(o any, c *gin.Context) {
	c.ShouldBindJSON(o)
	// if err := c.ShouldBindJSON(o); err != nil {
	// 	c.AbortWithStatusJSON(400, gin.H{"error": err.Error()})
	// 	return
	// }
}

type apiError struct {
	message string
}

func (a apiError) Error() string {
	return a.message
}

func (a apiError) RenderBadRequest(c *gin.Context) {
	c.AbortWithStatusJSON(400, gin.H{"error": a.Error()})
}

func (a apiError) RenderAuthorized(c *gin.Context) {
	c.AbortWithStatusJSON(403, gin.H{"error": a.Error()})
}

func (a apiError) RenderNotFound(c *gin.Context) {
	c.AbortWithStatusJSON(403, gin.H{"error": a.Error()})
}

func (a apiError) RenderInternalServer(c *gin.Context) {
	c.AbortWithStatusJSON(500, gin.H{"error": a.Error()})
}

func NewApiError(message string) *apiError {
	return &apiError{
		message: message,
	}
}
