package handlers

import (
	"fmt"
	"strconv"
	"github.com/gin-gonic/gin"

	model "bookmarks/internal/models"
	"bookmarks/internal/database"
)

func Bookmarks(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(model.User)

	bookmarks := []model.Bookmark{}
	database.Connection().Where("user_id = ?", currentUser.ID).Find(&bookmarks)

	c.JSON(200, gin.H{
		"message": fmt.Sprintf("Hello, %v", currentUser.Name),
		"bookmarks": bookmarks,
	})
}

func CreateBookmarks(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(model.User)

	bookmark := model.Bookmark{}
	bindWithObject(&bookmark, c)
	bookmark.UserID = currentUser.ID
	if err := bookmark.Validate(); len(err) > 0 {
		fmt.Println(err)
		err.RenderBadRequest(c)
		return
	}

	result := database.Connection().Create(&bookmark)
	
	if result.Error != nil {
		fmt.Println(result.Error)
		NewApiError(result.Error.Error()).RenderBadRequest(c)
		return
	}

	c.JSON(201, gin.H{
		"data": bookmark,
	})
}

func Bookmark(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(model.User)

	_, e := strconv.Atoi(c.Param("id"))
	if e != nil {
		NewApiError("ID is invalid").RenderBadRequest(c)
		return
	}

	bookmark := model.Bookmark{}
	if result := database.Connection().Where("id = ?", c.Param("id")).Where("user_id = ?", currentUser.ID).First(&bookmark); result.RowsAffected == 0 {
		NewApiError("Bookmark not found").RenderNotFound(c)
		return
	}

	c.JSON(200, gin.H{
		"data": bookmark,
	})
}

func UpdateBookmarks(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(model.User)

	_, e := strconv.Atoi(c.Param("id"))
	if e != nil {
		NewApiError("ID is invalid").RenderBadRequest(c)
		return
	}

	bookmark := model.Bookmark{}
	if result := database.Connection().Where("id = ?", c.Param("id")).Where("user_id = ?", currentUser.ID).First(&bookmark); result.RowsAffected == 0 {
		NewApiError("Bookmark not found").RenderNotFound(c)
		return
	}
	
	updateBookmark := model.UpdateBookmark{}
	bindWithObject(&updateBookmark, c)

	// TODO: Check if save and skip db call.

	updateBookmark.ID = bookmark.ID
	updateBookmark.UserID = bookmark.UserID

	if err := updateBookmark.Validate(); len(err) > 0 {
		fmt.Println(err)
		err.RenderBadRequest(c)
		return
	}

	database.Connection().Model(&bookmark).Updates(updateBookmark)

	c.JSON(200, gin.H{
		"data": bookmark,
	})
}

func DeleteBookmarks(c *gin.Context) {
	currentUser := c.MustGet("currentUser").(model.User)

	_, e := strconv.Atoi(c.Param("id"))
	if e != nil {
		NewApiError("ID is invalid").RenderBadRequest(c)
		return
	}

	bookmark := model.Bookmark{}
	if result := database.Connection().Where("id = ?", c.Param("id")).Where("user_id = ?", currentUser.ID).First(&bookmark); result.RowsAffected == 0 {
		NewApiError("Bookmark not found").RenderNotFound(c)
		return
	}

	database.Connection().Delete(&bookmark)

	c.JSON(200, gin.H{
		"data": bookmark,
	})
}
