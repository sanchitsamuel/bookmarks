package middleware

import (
	"github.com/gin-gonic/gin"
	"fmt"
	"strings"

	"bookmarks/internal/database"
	"bookmarks/internal/transport/http/handlers"
	service "bookmarks/internal/services"
	model "bookmarks/internal/models"
)

func Authenticate() gin.HandlerFunc {
	return func(c *gin.Context) {
		bearerToken := c.Request.Header.Get("Authorization")
		if bearerToken == "" {
			handlers.NewApiError("Unauthorized").RenderAuthorized(c)
		}
		reqToken := strings.Split(bearerToken, " ")[1]
		email, err := service.VerifyToken(reqToken)
		if err != nil {
			handlers.NewApiError("Unauthorized").RenderAuthorized(c)
		}
		fmt.Println(email)
		var currentUser = model.User{}
		result := database.Connection().Where("email = ?", email).First(&currentUser)
		if result.Error != nil || result.RowsAffected == 0 {
			handlers.NewApiError("Unauthorized").RenderAuthorized(c)
		}
		c.Set("currentUser", currentUser)
		c.Next()
	}
}