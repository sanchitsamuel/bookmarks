package services

import (
	"fmt"
	"github.com/golang-jwt/jwt/v5"
	"time"
	"errors"

	model "bookmarks/internal/models"
)

func secretKey() []byte {
	return []byte("secret-key")
}

func CreateToken(user *model.User) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, 
		jwt.MapClaims{
			"username": user.Email, 
			"exp": time.Now().Add(time.Hour * 24).Unix(), 
		},
	)

	tokenString, err := token.SignedString(secretKey())
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func VerifyToken(tokenString string) (string, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return secretKey(), nil
	})

	if err != nil {
		return "", err
	}

	if !token.Valid {
		return "", errors.New("Invalid token")
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	fmt.Println("Claims: ", claims)
	if ok && token.Valid {
				username := claims["username"].(string)
				return username, nil
	}

	return "", errors.New("unable to extract claims")
}