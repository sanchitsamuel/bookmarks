package config

import (
	"os"
)

type DatabaseConfig struct {
	Host string
	Name string
	User string
	Password string
}

func GetKey(key string) (string) {
	return os.Getenv(key)
}

func GetDBConfig() *DatabaseConfig {
	return &DatabaseConfig {
		Host: GetKey("DB_HOST"),
		Name: getDBName(),
		User: GetKey("DB_USER"),
		Password: GetKey("DB_PASS"),
	}
}

func getDBName() string {
	if GetEnv() == "TEST" {
		return GetKey("DB_TEST_NAME")
	}

	return GetKey("DB_NAME")
}

func GetEnv() string {
	// TODO: add allowed env to prevent bad values
	if len(os.Getenv("APP_ENV")) == 0 {
		return "DEV"
	}
	return os.Getenv("APP_ENV")
}
