package app

import (
	"bookmarks/internal/transport/http"
)

func Run()  {
	http.Router().Run()
}