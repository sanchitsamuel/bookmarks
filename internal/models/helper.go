package model

import(
	"strings"
	"github.com/gin-gonic/gin"
)

type attributeError struct {
	attr string
	message string
}

type ModelError []*attributeError;

func (a attributeError) Error() string {
	return a.message
}

func (m ModelError) hash() map[string]any {
	errorHash := make(map[string]any)
	for _, err := range m {
		errorHash[err.attr] = err.Error()
	}

	return errorHash
}

func (m ModelError) Error() string {
	errorStrings := make([]string, len(m))
	for _, err := range m {
		errorStrings = append(errorStrings, err.Error())
	}
	return strings.Join(errorStrings[:], ",")
}

func (m ModelError) RenderBadRequest(c *gin.Context) {
	c.AbortWithStatusJSON(400, gin.H{"errors": m.hash()})
}

func NewAttributeError(attr string, message string) *attributeError {
	return &attributeError {
		attr: attr,
		message: message,
	}
}
