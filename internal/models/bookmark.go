package model

import (
	"time"
	"regexp"
	"strconv"
	"gorm.io/gorm"
	"bookmarks/internal/database"
)

const (
	urlRegex = `https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)`
)

type Bookmark struct {
	ID        uint            `gorm:"primaryKey" json:"id"`
  CreatedAt time.Time				`json: "created_at"`
  UpdatedAt time.Time				`json: "updated_at"`
  DeletedAt gorm.DeletedAt  `gorm:"index" json:"deleted_at"`
	Title string 							`form:"title" json:"title"`
	Link string								`form:"link" json:"link"`
	UserID uint								`json:"user_id"`
}

func (b *Bookmark) Validate() (err ModelError) {
	// title
	if len(b.Title) == 0 {
		err = append(err, NewAttributeError("title", "is mandatory"))
	} else {
		bookmarks := []Bookmark{}
		database.Connection().Where("user_id = ?", b.UserID).Where("title = ?", b.Title).Find(&bookmarks)
		if len(bookmarks) > 0 {
			err = append(err, NewAttributeError("title", "is not unique"))
		}
	}

	// link
	validateLink(b.Link, &err)

	return
}

type UpdateBookmark struct {
	ID uint
	Title string
	Link string
	UserID uint
}

func (u *UpdateBookmark) Validate() (err ModelError) {
	// title
	if len(u.Title) == 0 {
		err = append(err, NewAttributeError("title", "is mandatory"))
	} else {
		bookmarks := []Bookmark{}
		database.Connection().Where("user_id = ?", u.UserID).Where("title = ?", u.Title).Not("id = ?", strconv.FormatUint(uint64(u.ID), 10)).Find(&bookmarks)
		if len(bookmarks) > 0 {
			err = append(err, NewAttributeError("title", "is not unique"))
		}
	}

	// link
	validateLink(u.Link, &err)

	return
}

func validateLink(link string, err *ModelError) {
	if len(link) == 0 {
		*err = append(*err, NewAttributeError("link", "is mandatory"))
	} else {
		match, e := regexp.MatchString(urlRegex, link)
		if e != nil || !match {
			*err = append(*err, NewAttributeError("link", "is not valid"))
		}
	}
}
