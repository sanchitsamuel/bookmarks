package model

import (
	"gorm.io/gorm"
	"golang.org/x/crypto/bcrypt"
	"time"
	"errors"
	"bookmarks/internal/database"
)

type User struct {
	ID        uint            `gorm:"primaryKey" json:"id"`
  CreatedAt time.Time				`json: "created_at"`
  UpdatedAt time.Time				`json: "updated_at"`
  DeletedAt gorm.DeletedAt  `gorm:"index" json:"deleted_at"`
	Name string 							`form:"name" json:"name binding:"required"`
	Email string 							`form:"email" json:"email" binding:"required" gorm:"index:idx_email,unique"`
	Password string						`form:"password" json:"password" binding:"required"`
	Bookmarks []*Bookmark			`json:",omitempty"`
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(u.Password), 14)
  if err != nil {
    err = errors.New("can't save invalid data")
  } else {
		u.Password = string(bytes)
	}
  return
}

type Login struct {
	Email string
	Password string
}

func (l *Login) Validate() (err ModelError) {
	if len(l.Email) == 0 || len(l.Password) == 0 {
		err = append(err, NewAttributeError("email", "Invalid email or password"))
	}
	
	return
}

type Signup struct {
	Name string
	Email string
	Password string
	ConfirmPassword string `json:"confirm_password"`
}

func (s *Signup) Validate() (err ModelError) {
	// Name
	if len(s.Name) == 0 {
		err = append(err, NewAttributeError("name", "is mandatory"))
	}

	// Email
	if len(s.Email) == 0 {
		err = append(err, NewAttributeError("email", "is mandatory"))
	} else {
		if result := database.Connection().Where("email = ?", s.Email).First(&User{}); result.RowsAffected > 0 {
			err = append(err, NewAttributeError("email", "is not unique"))
		}
	}

	// Password
	if len(s.Password) == 0 || len(s.ConfirmPassword) == 0 {
		err = append(err, NewAttributeError("password", "is mandatory"))
	} else {
		if s.Password != s.ConfirmPassword {
			err = append(err, NewAttributeError("password", "does not match"))
		}
	}
	
	return
}
